**Instructions:**

Import as java project in eclipse, then run as Java Application. 
Main class is TernarySearchTreeTest if no configuration is generated.

**Reserved Words**
You can enter these reserved words from the prompt to trigger the function.

printtree - prints all valid usernames inside the tree.

memory - prints memory usage of the application.

add - switches to "add username" mode. after a username is added, the program will revert back to default mode (recommend username).