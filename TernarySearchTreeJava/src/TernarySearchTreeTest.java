/**
 ** Java Program to Implement Ternary Search Tree
 **/

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class TernarySearchTreeTest {

	private BufferedReader reader;

	public void runTest() {
		try {
			// populate the tree
			System.out.println("Please wait. processing text file.");
			long time = System.nanoTime();
			try {
				reader = new BufferedReader(new FileReader("assets/usernames.txt"));
			} catch (FileNotFoundException f) {
				reader = new BufferedReader(new FileReader("usernames.txt"));
			}
			String s = null;
			TernarySearchTree tst = new TernarySearchTree();
			while ((s = reader.readLine()) != null) {
				tst.addUsername(s);
			}
			System.out.println("processing finished. time: " + (System.nanoTime() - time) + " ns");

			// now we can start suggesting usernames
			while (true) {
				BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
				System.out.print("enter username to search: ");
				String username = console.readLine();
				if (username.equals("printtree")) {
					System.out.println(tst.toString());
				} else if (username.equals("memory")) {
					DecimalFormat formatter = new DecimalFormat("#,###.##");
					System.out.println(formatter.format((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024 * 1024)) + " MB.");
				} else if (username.equals("add")) {
					System.out.print("Enter a username to add (this will only be added in the current session. The usernames file will not be modified.) :");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					String usernameToAdd = br.readLine();
					tst.addUsername(usernameToAdd);
					System.out.println("username " + usernameToAdd + " added.");
				} else {
					long newTime = System.nanoTime();
					int substringIndex = username.indexOf("@");
					if (substringIndex > 0) {
						username = username.substring(0, substringIndex);
					}
					username = username.replaceAll("\\d*$", "").toLowerCase().replace(" ", "");
					if (!tst.searchUsername(username)) {
						System.out.println("username " + username + " is available.");
					} else {
						// iterate
						int index = 1;
						String newWord = username + index;
						while (tst.searchUsername(newWord)) {
							newWord = username + index++;
						}
						System.out.println("username " + newWord + " is available.");
					}
					System.out.println("total search time: " + (System.nanoTime() - newTime) + " ns");
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) {
		new TernarySearchTreeTest().runTest();
	}
}